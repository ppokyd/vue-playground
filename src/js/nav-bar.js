/**
 * Each section id should be in format: "section-{index}"
 */
var app = new Vue({
  el: '#nav-bar',
  data: {
    activeIndex: 0,
    stacked: false
  },
  created () {
    this.activeIndex = this.getActiveSectionIndex();
    window.addEventListener('scroll', this.handleScroll.bind(this));
  },
  destroyed () {
    window.removeEventListener('scroll', this.handleScroll.bind(this));
  },
  methods: {
    handleScroll: function() {
      this.stacked = window.scrollY > 100;
      this.activeIndex = this.getActiveSectionIndex();
    },
    goTo: function(index) {
      const container = document.getElementById('section-' + index);

      if (container) {
        container.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
      }
    },
    getActiveSectionIndex: function() {
      const sections = document.querySelectorAll('div[id*="section-"');
      let index;

      Array.from(sections).forEach((section, i) => {
        if (!index && this.isInViewport(section)) {
          index = i + 1;
        }
      });

      return index || 0;
    },
    isInViewport: function (elem) {
      const bounding = elem.getBoundingClientRect();
      const wHeight = window.innerHeight || document.documentElement.clientHeight

      return bounding.top >= 0 || bounding.bottom > wHeight / 2;
    }
  }
});