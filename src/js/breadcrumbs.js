var app = new Vue({
  el: '#breadcrumbs',
  data: {
    activeIndex: 1
  },
  mounted() {
    this.max = this.$el.querySelectorAll('.breadcrumbs li').length;
  },
  methods: {
    next: function() {
      if (this.activeIndex + 1 <= this.max) {
        this.activeIndex = this.activeIndex + 1;
      }
    },
    prev: function() {
      if (this.activeIndex - 1 > 0) {
        this.activeIndex = this.activeIndex - 1;
      }
    }
  }
});