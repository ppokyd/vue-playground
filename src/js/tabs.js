var app = new Vue({
  el: '#tabs',
  data: {
    activeIndex: 1
  },
  methods: {
    switchTab: function(index) {
      this.activeIndex = index;
    }
  }
});